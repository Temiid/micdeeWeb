import React, { Component } from 'react';

import '../App.css';
import abstract from '../assets/abstract.svg'

class Comp2 extends Component {
    render() {
        return (
            <div className="App">

                <div className="comp2">
                    <div className="comp2a">
                        <div className='abstract'>
                            <h3 className='header'>Transitioning from abstract ideas to concrete visuals.</h3>
                            <p className='bodyText'>We provide amazing presentations and visuals that make the creative process more communicative. We ensure we put designs on display in a manner that best communicates the identity and ideals of the client.</p>
                            <div className='button'>Learn More</div>
                        </div>
                        <img className='abstractImage' src={abstract} alt='Micdee-logo' />
                    </div>
                </div>
            </div>
        );
    }
}

export default Comp2;

import React, { Component } from 'react';

import '../App.css';
import slide1 from '../assets/slider1.svg'
import logos from '../assets/logo.svg'

class Comp1 extends Component {
    render() {
        return (
            <div className="App">

                <div className="comp1">

                    <div className="comp1a">
                        <div className="nav">

                            <div className="logo">
                                <img src={logos} alt='Micdee-logo' />
                            </div>

                            <ul className="menu">

                                <li class="nav-links"><a href="#">STUDIO</a></li>
                                <li class="nav-links"><a href="#"> CAPABILITIES</a></li>
                                <li class="nav-links"><a href="#">WORK</a></li>
                                <li class="nav-links"><a href="#">CONTACT</a></li>
                            </ul>

                        </div>
                    </div>

                    <div className="slide">

                        <div className='slideText'>
                            <div style={{fontSize: 13, padding: 0, color: 'rgba(255,255,255,0.6)'}}>OUR VALUE PROPOSITION</div>
                            <div style={{fontWeight: 500, fontSize: 34, paddingTop: 10}}>Bridging the gap between designers and clients </div>

                            <div style={{paddingTop: 15, fontSize: 13}} className='slideSelector'>
                                <div style={{paddingTop: 6, color: 'rgba(255,255,255,0.6)'}}>FROM PROJECT</div>
                                <div style={{backgroundColor: 'white',padding: 6, color:'#9D2B4A', marginLeft: 20,fontWeight:500}}>COUNTRY HOME</div>
                            </div>

                            <div style={{paddingTop: 40}} className='indicators'>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'#9D2B4A', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                            </div>
                        </div>

                        <img src={slide1} className="slider" alt='slider-img'/>
                    </div>


                </div>
            </div>
        );
    }
}

export default Comp1;

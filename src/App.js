import React, { Component } from 'react';
import './App.css';
import Comp1 from './components/comp1'
import Comp2 from './components/comp2'
import Comp3 from './components/comp3'
import Comp4 from './components/comp4'
import Comp5 from './components/comp5'
class App extends Component {
  render() {
    return (
      <div className="App">
      
       <Comp1 />
       <Comp2 />
       <Comp3 />
       <Comp4 />
       <Comp5 />
      </div>
    );
  }
}

export default App;
